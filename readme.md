# Elastic Logstash Kibana on Docker Lab

## Project description
Hands on lab for streamed bootcamp of BTPN CHIP 10 trainee - digital infrastructure.

## How to setup your local env?
1. Edit elasticsearch, kibana_system, and logstash_internal default password at .env
2. Run docker-compose up -d

## How to open the Kibana?
After some moments after the docker image is built, open your browser at: http://localhost:5601. Enter username elastic and password as you specify at .env

## How to add new pipeline on logstash?
1. Add new logstash config at ./logstash/pipelines
2. Add your new pipeline path at ./logstash/config/pipelines.yml